<?php

$admin_email="john@smith.com";
$admin_nick="JohnSmith";

// nickname of your bot
$irpg_bot="IdleRPG";

// your game's server
$irpg_network="irc.smith.com";

// your game's channel
$irpg_chan="#IdleRPG";

// full or relative pathname to the DBs:

// character database
$irpg_db="~/irpg.db";

// time modifiers file
$irpg_mod="~/modifiers.txt";

// active quest info file
$irpg_qfile="~/questinfo.txt";

// image to use for the top logo
$irpg_logo="idlerpg.png";

// directory in which your site is located from the root directory. my site
// is http://jotun.ultrazone.org/g7/, so it's "/g7/"
$BASEURL="/";

// width-wise dimension of your map file
$mapx = 500;

// length-wise dimension of your map file
$mapy = 500;

?>
