<?php

    include("config.php");
    
    $irpg_page_title = "DB-style Player Listing";
    
    include("header.php");
    
    include("commonfunctions.php");

    $file = file($irpg_db);
    unset($file[0]);
    if (!$_GET['sort'] ||
        (($_GET['sort'] != "cmp_level_asc") &&
        ($_GET['sort'] != "cmp_level_desc") &&
        ($_GET['sort'] != "cmp_isadmin_asc") &&
        ($_GET['sort'] != "cmp_isadmin_desc") &&
        ($_GET['sort'] != "cmp_alignment_asc") &&
        ($_GET['sort'] != "cmp_alignment_desc") &&
        ($_GET['sort'] != "cmp_ttl_asc") &&
        ($_GET['sort'] != "cmp_ttl_desc") &&
        ($_GET['sort'] != "cmp_pen_asc") &&
        ($_GET['sort'] != "cmp_pen_desc") &&
        ($_GET['sort'] != "cmp_lastlogin_asc") &&
        ($_GET['sort'] != "cmp_lastlogin_desc") &&
        ($_GET['sort'] != "cmp_created_asc") &&
        ($_GET['sort'] != "cmp_created_desc") &&
        ($_GET['sort'] != "cmp_idled_asc") &&
        ($_GET['sort'] != "cmp_idled_desc") &&
        ($_GET['sort'] != "cmp_user_asc") &&
        ($_GET['sort'] != "cmp_user_desc") &&
        ($_GET['sort'] != "cmp_online_asc") &&
        ($_GET['sort'] != "cmp_online_desc") &&
        ($_GET['sort'] != "cmp_sum_asc") &&
        ($_GET['sort'] != "cmp_sum_desc"))) $_GET['sort'] = "cmp_level_desc";
    usort($file,$_GET['sort']);
?>
    <table border=1 cellpadding=2 cellspacing=2 cols="32" rows="<?php print count($file); ?>">
      <tr>
        <th NOWRAP>
          User
          (<a href="db.php?sort=cmp_user_asc">
             <img src="up.png" border="0">
           </a>
           /
          <a href="db.php?sort=cmp_user_desc">
             <img src="down.png" border="0">
          </a>)
        </th>
        <th NOWRAP>
          Level
          (<a href="db.php?sort=cmp_level_asc">
             <img src="up.png" border="0">
           </a>
           /
          <a href="db.php?sort=cmp_level_desc">
             <img src="down.png" border="0">
          </a>)
        </th>
        <th NOWRAP>
          Admin
          (<a href="db.php?sort=cmp_isadmin_asc">
             <img src="up.png" border="0">
           </a>
           /
          <a href="db.php?sort=cmp_isadmin_desc">
             <img src="down.png" border="0">
          </a>)
        </th>
        <th NOWRAP>Class</th>
        <th NOWRAP>
          TTL
          (<a href="db.php?sort=cmp_ttl_asc">
             <img src="up.png" border="0">
           </a>
           /
          <a href="db.php?sort=cmp_ttl_desc">
             <img src="down.png" border="0">
          </a>)
        </th>
        <th NOWRAP>
          Online
          (<a href="db.php?sort=cmp_online_asc">
             <img src="up.png" border="0">
           </a>
           /
          <a href="db.php?sort=cmp_online_desc">
             <img src="down.png" border="0">
          </a>)
        </th>
        <th NOWRAP>
          Total Time Idled
          (<a href="db.php?sort=cmp_idled_asc">
             <img src="up.png" border="0">
           </a>
           /
          <a href="db.php?sort=cmp_idled_desc">
             <img src="down.png" border="0">
          </a>)
        </th>
        <th NOWRAP>
          Total Pen.
          (<a href="db.php?sort=cmp_pen_asc">
             <img src="up.png" border="0">
           </a>
           /
          <a href="db.php?sort=cmp_pen_desc">
             <img src="down.png" border="0">
          </a>)
        </th>
        <th NOWRAP>
          Acct. Created
          (<a href="db.php?sort=cmp_created_asc">
             <img src="up.png" border="0">
           </a>
           /
          <a href="db.php?sort=cmp_created_desc">
             <img src="down.png" border="0">
          </a>)
        </th>
        <th NOWRAP>
          Last Login
          (<a href="db.php?sort=cmp_lastlogin_asc">
             <img src="up.png" border="0">
           </a>
           /
          <a href="db.php?sort=cmp_lastlogin_desc">
             <img src="down.png" border="0">
          </a>)
        </th>
        <th NOWRAP>
          Sum
          (<a href="db.php?sort=cmp_sum_asc">
             <img src="up.png" border="0">
           </a>
           /
          <a href="db.php?sort=cmp_sum_desc">
             <img src="down.png" border="0">
          </a>)
        </th>
        <th NOWRAP>
          Alignment
          (<a href="db.php?sort=cmp_alignment_asc">
             <img src="up.png" border="0">
           </a>
           /
          <a href="db.php?sort=cmp_alignment_desc">
             <img src="down.png" border="0">
          </a>)
        </th>
      </tr>
<?php
    foreach ($file as $line) {
      list($user,$isadmin,$level,$class,$secs,$online,$idled,
           $created,
           $lastlogin,
           $alignment,
          ) = explode("\t",trim($line));
      $class = str_replace("<","&lt;",$class);
      $user = str_replace("<","&lt;",$user);
      $class = str_replace(">","&gt;",$class);
      $user = str_replace(">","&gt;",$user);
      $sum = 0;
      foreach ($item as $k => $v) $sum += $v;
      $pentot = 0;
      foreach ($pen as $k => $v) $pentot += $v;
      echo "      <tr>\n".
           "        <td nowrap>$user</td>\n".
           "        <td>$level</td>\n".
           "        <td>".($isadmin?"Yes":"No")."</td>\n".
           "        <td nowrap>$class</td>\n".
           "        <td nowrap>".duration($secs)."</td>\n".
           "        <td>".(($online == 1) ? "Yes" : "No")."</td>\n".
           "        <td nowrap>".duration($idled)."</td>\n".
           "        <td nowrap>$x</td>\n".
           "        <td nowrap>$y</td>\n".
           "        <td nowrap>".duration($pentot)."</td>\n".
           "        <td nowrap>".date("D M j H:i:s Y",$created)."</td>\n".
           "        <td nowrap>".date("D M j H:i:s Y",$lastlogin)."</td>\n".
           "        <td>$sum</td>\n".
           "        <td>".($alignment=='e'?"Evil":($alignment=='n'?"Neutral":"Good"))."</td>\n".
           "      </tr>\n";
    }

    echo('
    </table>
    <br><br>
    * Accounts created before Aug 29, 2003 may have incowrect data fields.
    ');
    include("footer.php");
?>
